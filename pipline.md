| HAP | permanent archive addresses |
| - | - |
| Calc_Demo.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Calc_Demo/20221207_093308/version-Master_Version-hap_Calc_Demo-20221207_093308-hap_Calc_Demo.tar.gz |
| CallUI.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_CallUI/20221124_163021/version-Master_Version-hap_CallUI-20221124_163021-hap_CallUI.tar.gz |
| Camera.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Camera_with_sdk/20230228_144337/version-Master_Version-hap_Camera_with_sdk-20230228_144337-hap_Camera_with_sdk.tar.gz |
| CertificateManager.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_CertificateManager_with_sdk/20230321_210834/version-Master_Version-hap_CertificateManager_with_sdk-20230321_210834-hap_CertificateManager_with_sdk.tar.gz |
| Clock_Demo.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Clock_Demo/20221206_214559/version-Master_Version-hap_Clock_Demo-20221206_214559-hap_Clock_Demo.tar.gz |
| Contacts.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Contacts_with_sdk/20230322_154052/version-Master_Version-hap_Contacts_with_sdk-20230322_154052-hap_Contacts_with_sdk.tar.gz |
| FilePicker.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_FilePicker_with_sdk/20230216_144737/version-Master_Version-hap_FilePicker_with_sdk-20230216_144737-hap_FilePicker_with_sdk.tar.gz |
| Launcher.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Launcher_with_sdk/20230323_124132/version-Master_Version-hap_Launcher_with_sdk-20230323_124132-hap_Launcher_with_sdk.tar.gz |
| Launcher_Settings.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Launcher_with_sdk/20230323_124132/version-Master_Version-hap_Launcher_with_sdk-20230323_124132-hap_Launcher_with_sdk.tar.gz |
| Mms.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Mms_with_sdk/20230322_154116/version-Master_Version-hap_Mms_with_sdk-20230322_154116-hap_Mms_with_sdk.tar.gz |
| MobileDataSettings.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_CallUI/20221124_163021/version-Master_Version-hap_CallUI-20221124_163021-hap_CallUI.tar.gz |
| Music_Demo.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Music_Demo_with_sdk/20230215_092502/version-Master_Version-hap_Music_Demo_with_sdk-20230215_092502-hap_Music_Demo_with_sdk.tar.gz |
| Note.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Note_with_sdk/20230316_185910/version-Master_Version-hap_Note_with_sdk-20230316_185910-hap_Note_with_sdk.tar.gz |
| Photos.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Photos/20230317_111855/version-Master_Version-hap_Photos-20230317_111855-hap_Photos.tar.gz |
| ScreenShot.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_ScreenShot/20221124_163242/version-Master_Version-hap_ScreenShot-20221124_163242-hap_ScreenShot.tar.gz |
| Settings.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Settings_with_sdk/20230317_100848/version-Master_Version-hap_Settings_with_sdk-20230317_100848-hap_Settings_with_sdk.tar.gz |
| SettingsData.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_SettingsData_with_sdk/20230317_101240/version-Master_Version-hap_SettingsData_with_sdk-20230317_101240-hap_SettingsData_with_sdk.tar.gz |
| Settings_FaceAuth.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Settings_FaceAuth/20221212_164352/version-Master_Version-hap_Settings_FaceAuth-20221212_164352-hap_Settings_FaceAuth.tar.gz |
| SystemUI-DropdownPanel.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_SystemUI_with_sdk/20230320_191849/version-Master_Version-hap_SystemUI_with_sdk-20230320_191849-hap_SystemUI_with_sdk.tar.gz |
| SystemUI-NavigationBar.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_SystemUI_with_sdk/20230320_191849/version-Master_Version-hap_SystemUI_with_sdk-20230320_191849-hap_SystemUI_with_sdk.tar.gz |
| SystemUI-NotificationManagement.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_SystemUI_with_sdk/20230320_191849/version-Master_Version-hap_SystemUI_with_sdk-20230320_191849-hap_SystemUI_with_sdk.tar.gz |
| SystemUI-ScreenLock.hap | http://download.ci.openharmony.cn/version/Daily_Version/hap_ScreenLock_with_sdk/20230323_192116/version-Daily_Version-hap_ScreenLock_with_sdk-20230323_192116-hap_ScreenLock_with_sdk.tar.gz |
| SystemUI-StatusBar.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_SystemUI_with_sdk/20230320_191849/version-Master_Version-hap_SystemUI_with_sdk-20230320_191849-hap_SystemUI_with_sdk.tar.gz |
| SystemUI-VolumePanel.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_SystemUI_with_sdk/20230320_191849/version-Master_Version-hap_SystemUI_with_sdk-20230320_191849-hap_SystemUI_with_sdk.tar.gz |
| SystemUI.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_SystemUI_with_sdk/20230320_191849/version-Master_Version-hap_SystemUI_with_sdk-20230320_191849-hap_SystemUI_with_sdk.tar.gz |
| kikaInput.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_kikaInput_with_sdk/20230224_141118/version-Master_Version-hap_kikaInput_with_sdk-20230224_141118-hap_kikaInput_with_sdk.tar.gz |

| SDK | optional download urls |
| - | - |
| 3.1.7.7 | https://mirrors.huaweicloud.com/openharmony/os/3.1.2/sdk-patch/ohos-sdk-full.tar.gz |
| 3.2.7.5 | https://repo.huaweicloud.com/harmonyos/os/3.2-Beta3/ohos-sdk-windows_linux-full.tar.gz |
| 3.2.7.6 | https://repo.huaweicloud.com/harmonyos/os/3.2-Beta3/sdk-patch/ohos-sdk-full.tar.gz |
| 3.2.8.3 | http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221031_100640/version-Master_Version-OpenHarmony_3.2.8.3-20221031_100640-ohos-sdk-full.tar.gz |
